function renderProduct(product) {
  let contentHTML = "";

  product.forEach(function (item) {
    contentHTML += `
    <div class="col-lg-3 col-md-6 pt-5">
    <div class="card text-center h-100 p-3">
    <img style="width: 200px;"  class="card-img-top " src=${item.img} alt="">
    <div class="card-body">
      <h4 class="card-title">${item.name}</h4>
      <p class="card-text">${item.desc}</p>
      </div>
      <button id="addToCart" onclick="addToCart(${item.id})" class="btn ">Add To Cart</button>
  </div>
  </div>
    
    `;
  });
  document.getElementById("listPhone").innerHTML = contentHTML;
}

// --------------------------

function renderCart(cart) {
  let contentHTML = "";

  cart.forEach(function (item) {
    contentHTML += `
    <tr>
    <td>
    <img src="${item.img}" width="100px"  />
    </td>
   
    <td>${item.name}</td>
    <td>
    <i onclick= "decrease(${item.id})" class="fa fa-angle-left"></i>
   <button class="btn btn-outline-secondary">${item.quantity}</button>
    <i onclick= "increase(${item.id})" class="fa fa-angle-right"></i>
    </td>
    <td>${item.price * item.quantity}$</td>
    
    
    <td>
    
    <i id="removeCart" onclick="removeCart(${item.id})" class="fa fa-times"></i>
    </td>
    
    </tr>
    
    
    
    `;
  });
  document.getElementById("tbody-cart").innerHTML = contentHTML;
}
