class Product {
  constructor(name, img, price, quantity) {
    this.name = name;
    this.img = img;
    this.price = price;
    this.quantity = quantity;
  }
}
