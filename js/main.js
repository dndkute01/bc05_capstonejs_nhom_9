const BASE_URL = "https://636ddd2a91576e19e332079b.mockapi.io";

let productList = [];
const PRODUCT = "PRODUCT";
let cart = [];

function fetchAllProduct() {
  axios({
    url: `${BASE_URL}/product`,
    method: "GET",
  })
    .then(function (res) {
      res.data.forEach(function (item) {
        productList.push(item);
      });

      renderProduct(res.data);
      totalPrice();

      // console.log(res.data[0].type);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
}

fetchAllProduct();

//-----------------------------------Lưu vào localStorage cho cart

function saveLocalStorage(product) {
  let jsonProduct = JSON.stringify(product);

  localStorage.setItem(PRODUCT, jsonProduct);
}

let dataJson = localStorage.getItem(PRODUCT);
if (dataJson !== null) {
  let productArr = JSON.parse(dataJson);

  for (var i = 0; i < productArr.length; i++) {
    var item = productArr[i];
    cart.push(item);
    renderCart(cart);
  }
}
//--------------------------------------

// ----Lọc danh sách theo option Iphone và Samsung

const selectElement = document.querySelector("#slectPhone");
selectElement.addEventListener("change", function (event) {
  var isChoice = event.target.value;
  if (isChoice !== "Select product") {
    axios({
      url: `${BASE_URL}/product`,
      method: "GET",
    })
      .then(function (res) {
        let typeArr = [];
        // gán mảng data vào biến
        var productArr = res.data;
        // console.log("productArr: ", productArr);
        // Chạy vòng lặp
        productArr.forEach((item) => {
          // Dùng điều kiện type để lọc
          if (item.type == isChoice) {
            typeArr.push(item);

            renderProduct(typeArr);
          }
        });
      })
      .catch(function (err) {
        console.log("err: ", err);
      });
  } else {
    fetchAllProduct();
  }
});

// findlocation
function findLocation(id, nvArr) {
  for (var index = 0; index < nvArr.length; index++) {
    var item = nvArr[index];
    if (item.account == id) {
      // return function
      return index;
    }
  }
  // if not find out, return -1
  return -1;
}

// --Thêm vào giỏ hàng

// var cartItem = {
//   product: { name: "Sam sung", img: "#", price: 1000 },
//   quantity: 1,
// };

function addToCart(id) {
  var infoProduct = null;
  axios({
    url: `${BASE_URL}/product/${id}`,
    method: "GET",
  })
    .then((res) => {
      infoProduct = res.data;

      cart.push(infoProduct);
      saveLocalStorage(cart);
      totalPrice();
      renderCart(cart);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
}

//---remove sản phẩm khỏi cart
function removeCart(id) {
  for (let i = 0; i < cart.length; i++) {
    var cartArr = cart[i];

    if (cartArr.id == id) {
      cart.splice(i, 1);
    }
  }

  saveLocalStorage(cart);
  totalPrice();
  renderCart(cart);
}

// ----Thay đổi số lượng sản phẩm trong danh sách
function decrease(id) {
  for (let i = 0; i < cart.length; i++) {
    var cartArr = cart[i];
    if (cartArr.id == id) {
      cartArr.quantity--;
    }
    if (cartArr.quantity == 0) {
      removeCart(id);
    }
    // console.log(cartArr.quantity);
  }
  saveLocalStorage(cart);
  totalPrice();
  renderCart(cart);
}

function increase(id) {
  for (let i = 0; i < cart.length; i++) {
    var cartArr = cart[i];
    if (cartArr.id == id) {
      cartArr.quantity++;
    }

    // console.log(cartArr.quantity);
  }
  saveLocalStorage(cart);
  totalPrice();
  renderCart(cart);
}

// --Thay đổi số tiền sản phẩm
function totalPrice() {
  let totalMoney = 0;
  for (let i = 0; i < cart.length; i++) {
    var p = cart[i];
    totalMoney += p.quantity * p.price;
  }

  document.getElementById("total").innerHTML = `Total: ${totalMoney}$`;
}

// --Xóa toàn bộ sản phẩm trong cart
function clearCart() {
  console.log("yes");
}
